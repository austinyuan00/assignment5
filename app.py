#!/usr/bin/env python3

#-----------
# imports
#-----------

from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
import json

app = Flask(__name__)

booksList = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]
missingIDs = []

@app.route('/book/JSON/')
def bookJSON():
	r = json.dumps(booksList)
	loaded_r = json.loads(r)
	return jsonify(loaded_r)

@app.route('/')
@app.route('/book/')
def showBook():
	return render_template('showBook.html', books = booksList)


@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
	if request.method == 'POST':
		newBookTitle = request.form['name']
		if newBookTitle != "":
			if len(missingIDs) > 0:
				booksList.append({'title': newBookTitle, 'id': missingIDs.pop()})
			else:
				booksList.append({'title': newBookTitle, 'id': len(booksList) + 1})
		return redirect(url_for('showBook'))
	else:
		return render_template('newBook.html', books = booksList)

@app.route('/book/<int:book_id>/<title>/edit/', methods=['GET','POST'])
def editBook(book_id, title):
	if request.method == 'POST':
		changedTitle = request.form['name']
		if changedTitle == "":
			return redirect(url_for('showBook'))
		for b in booksList:
			if int(b['id']) == book_id:
				b['title'] = changedTitle
				return redirect(url_for('showBook'))
	else:
		return render_template('editBook.html', book_id = book_id, title = title, books = booksList)



@app.route('/book/<int:book_id>/<title>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id, title):
	if request.method == 'POST':
		for b in booksList:
			if int(b['id']) == book_id:
				booksList.remove(b)
				missingIDs.insert(0, book_id)
				return redirect(url_for('showBook'))
	else:
		return render_template('deleteBook.html', book_id = book_id, title = title, books = booksList)

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)
